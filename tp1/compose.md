# III. Docker compose

🌞 **Créez un fichier `docker-compose.yml`**

```bash
[baptiste@docker compose_test]$ sudo cat docker-compose.yml
version: "3"

services:
  conteneur_nul:
    image: debian
    entrypoint: sleep 9999
  conteneur_flopesque:
    image: debian
    entrypoint: sleep 9999
```

🌞 **Lancez les deux conteneurs avec docker compose**

```
[baptiste@docker compose_test]$ docker compose up -d
[+] Running 3/3
 ✔ conteneur_flopesque 1 layers [⣿]      0B/0B      Pulled                                                         3.2s
   ✔ bc0734b949dc Already exists                                                                                   0.0s
 ✔ conteneur_nul Pulled                                                                                            3.5s
[+] Running 3/3
 ✔ Network compose_test_default                  Created                                                           0.2s
 ✔ Container compose_test-conteneur_flopesque-1  Started                                                           0.0s
 ✔ Container compose_test-conteneur_nul-1        Started
```

🌞 **Vérifier que les deux conteneurs tournent**

```
[baptiste@docker compose_test]$  docker compose ps
NAME                                 IMAGE     COMMAND        SERVICE               CREATED          STATUS          PORTS
compose_test-conteneur_flopesque-1   debian    "sleep 9999"   conteneur_flopesque   58 seconds ago   Up 57 seconds
compose_test-conteneur_nul-1         debian    "sleep 9999"   conteneur_nul         58 seconds ago   Up 57 seconds
```

🌞 **Pop un shell dans le conteneur conteneur_nul**

```

[baptiste@docker compose_test]$ sudo cat docker-compose.yml
version: "3"

services:
  conteneur_nul:
    container_name: conteneur_nul
    image: debian
    entrypoint: sleep 9999
  conteneur_flopesque:
    container_name: conteneur_flopesque
    image: debian
    entrypoint: sleep 9999

    
[baptiste@docker compose_test]$ docker exec -it conteneur_nul /bin/bash


root@ebec9c9e7e84:/# apt-get update
Get:1 http://deb.debian.org/debian bookworm InRelease [151 kB]
Get:2 http://deb.debian.org/debian bookworm-updates InRelease [52.1 kB]
Get:3 http://deb.debian.org/debian-security bookworm-security InRelease [48.0 kB]
Get:4 http://deb.debian.org/debian bookworm/main amd64 Packages [8787 kB]
Get:5 http://deb.debian.org/debian bookworm-updates/main amd64 Packages [12.7 kB]
Get:6 http://deb.debian.org/debian-security bookworm-security/main amd64 Packages [132 kB]
Fetched 9183 kB in 2s (4397 kB/s)
Reading package lists... Done


root@ebec9c9e7e84:/# apt-get install -y iputils-ping
Reading package lists... Done
Building dependency tree... Done
Reading state information... Done
The following additional packages will be installed:
  libcap2-bin libpam-cap
The following NEW packages will be installed:
  iputils-ping libcap2-bin libpam-cap
0 upgraded, 3 newly installed, 0 to remove and 0 not upgraded.
Need to get 96.2 kB of archives.



root@ebec9c9e7e84:/# ping conteneur_flopesque
PING conteneur_flopesque (172.18.0.2) 56(84) bytes of data.
64 bytes from conteneur_flopesque.compose_test_default (172.18.0.2): icmp_seq=1 ttl=64 time=0.068 ms
64 bytes from conteneur_flopesque.compose_test_default (172.18.0.2): icmp_seq=2 ttl=64 time=0.073 ms
64 bytes from conteneur_flopesque.compose_test_default (172.18.0.2): icmp_seq=3 ttl=64 time=0.075 ms
64 bytes from conteneur_flopesque.compose_test_default (172.18.0.2): icmp_seq=4 ttl=64 time=0.074 ms
^C
--- conteneur_flopesque ping statistics ---
4 packets transmitted, 4 received, 0% packet loss, time 3027ms
rtt min/avg/max/mdev = 0.068/0.072/0.075/0.002 ms
```