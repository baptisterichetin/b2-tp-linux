# I. Init


## 3. sudo c pa bo


🌞 **Ajouter votre utilisateur au groupe `docker`**

```bash
[baptiste@docker ~]$ sudo usermod -aG docker baptiste
[sudo] password for baptiste:

[baptiste@docker ~]$ docker ps
CONTAINER ID   IMAGE     COMMAND   CREATED   STATUS    PORTS     NAMES
```

## 4. Un premier conteneur en vif


🌞 **Lancer un conteneur NGINX**


```bash
[baptiste@docker ~]$ docker run -d -p 9999:80 nginx
Unable to find image 'nginx:latest' locally
latest: Pulling from library/nginx
af107e978371: Pull complete
336ba1f05c3e: Pull complete
8c37d2ff6efa: Pull complete
51d6357098de: Pull complete
782f1ecce57d: Pull complete
5e99d351b073: Pull complete
7b73345df136: Pull complete
Digest: sha256:bd30b8d47b230de52431cc71c5cce149b8d5d4c87c204902acf2504435d4b4c9
Status: Downloaded newer image for nginx:latest
0fe11202d46a183697ed7183ce421f7ad51c1e820097288467131ce42f0fb721

```


🌞 **Visitons**


```bash
[baptiste@docker ~]$ docker ps
CONTAINER ID   IMAGE     COMMAND                  CREATED              STATUS              PORTS                                   NAMES
0fe11202d46a   nginx     "/docker-entrypoint.…"   About a minute ago   Up About a minute   0.0.0.0:9999->80/tcp, :::9999->80/tcp   great_pascal


[baptiste@docker ~]$ docker logs great_pascal
/docker-entrypoint.sh: /docker-entrypoint.d/ is not empty, will attempt to perform configuration
/docker-entrypoint.sh: Looking for shell scripts in /docker-entrypoint.d/
/docker-entrypoint.sh: Launching /docker-entrypoint.d/10-listen-on-ipv6-by-default.sh
10-listen-on-ipv6-by-default.sh: info: Getting the checksum of /etc/nginx/conf.d/default.conf
10-listen-on-ipv6-by-default.sh: info: Enabled listen on IPv6 in /etc/nginx/conf.d/default.conf
/docker-entrypoint.sh: Sourcing /docker-entrypoint.d/15-local-resolvers.envsh
/docker-entrypoint.sh: Launching /docker-entrypoint.d/20-envsubst-on-templates.sh
/docker-entrypoint.sh: Launching /docker-entrypoint.d/30-tune-worker-processes.sh
/docker-entrypoint.sh: Configuration complete; ready for start up
2023/12/21 15:06:19 [notice] 1#1: using the "epoll" event method
2023/12/21 15:06:19 [notice] 1#1: nginx/1.25.3
2023/12/21 15:06:19 [notice] 1#1: built by gcc 12.2.0 (Debian 12.2.0-14)
2023/12/21 15:06:19 [notice] 1#1: OS: Linux 5.14.0-362.13.1.el9_3.x86_64
2023/12/21 15:06:19 [notice] 1#1: getrlimit(RLIMIT_NOFILE): 1073741816:1073741816
2023/12/21 15:06:19 [notice] 1#1: start worker processes
2023/12/21 15:06:19 [notice] 1#1: start worker process 28


[baptiste@docker ~]$ docker inspect great_pascal
[
    {
        "Id": "0fe11202d46a183697ed7183ce421f7ad51c1e820097288467131ce42f0fb721",
        "Created": "2023-12-21T15:06:19.404486722Z",
        "Path": "/docker-entrypoint.sh",
        "Args": [
            "nginx",
            "-g",
            "daemon off;"
        ],
        "State": {
            "Status": "running",
            "Running": true,
            "Paused": false,
            "Restarting": false,
            "OOMKilled": false,
            "Dead": false,
            "Pid": 6594,
            "ExitCode": 0,
            "Error": "",
            "StartedAt": "2023-12-21T15:06:19.795010695Z",
            "FinishedAt": "0001-01-01T00:00:00Z"
        },


[baptiste@docker ~]$ sudo firewall-cmd --add-port=9999/tcp --permanent
[sudo] password for baptiste:
success
[baptiste@docker ~]$ sudo firewall-cmd --reload
success

[baptiste@docker ~]$ ss -lnpt
State     Recv-Q    Send-Q         Local Address:Port         Peer Address:Port    Process
LISTEN    0         128                  0.0.0.0:22                0.0.0.0:*
LISTEN    0         4096                 0.0.0.0:9999              0.0.0.0:*
LISTEN    0         128                     [::]:22                   [::]:*
LISTEN    0         4096                    [::]:9999                 [::]:*



[baptiste@docker ~]$ curl 10.1.1.10:9999
<!DOCTYPE html>
<html>
<head>
<title>Welcome to nginx!</title>
<style>
html { color-scheme: light dark; }
body { width: 35em; margin: 0 auto;
font-family: Tahoma, Verdana, Arial, sans-serif; }
</style>
</head>
<body>
<h1>Welcome to nginx!</h1>
<p>If you see this page, the nginx web server is successfully installed and
working. Further configuration is required.</p>

<p>For online documentation and support please refer to
<a href="http://nginx.org/">nginx.org</a>.<br/>
Commercial support is available at
<a href="http://nginx.com/">nginx.com</a>.</p>

<p><em>Thank you for using nginx.</em></p>
</body>
</html>

```


🌞 **On va ajouter un site Web au conteneur NGINX**


```bash
[baptiste@docker nginx]$ sudo cat index.html
<h1>MEOOOW</h1>
[baptiste@docker nginx]$ sudo cat site_nul.conf
server {
    listen        8080;

    location / {
        root /var/www/html;
    }
}

[baptiste@docker nginx]$ docker run -d -p 9999:8080 -v /home/baptiste/nginx/index.html:/var/www/html/index.html -v /home/baptiste/nginx/site_nul.conf:/etc/nginx/conf.d/site_nul.conf nginx
21888e204e3adc4d40ee5f395a96ee46e5276823d4242c82d04ed8a99ebab049

```

🌞 **Visitons**

```bash
[baptiste@docker nginx]$ docker ps
CONTAINER ID   IMAGE     COMMAND                  CREATED         STATUS        PORTS                                               NAMES
21888e204e3a   nginx     "/docker-entrypoint.…"   2 seconds ago   Up 1 second   80/tcp, 0.0.0.0:9999->8080/tcp, :::9999->8080/tcp   funny_leakey

[baptiste@docker nginx]$ curl 10.1.1.10:9999
<h1>MEOOOW</h1>
```

## 5. Un deuxième conteneur en vif


🌞 **Lance un conteneur Python, avec un shell**


```bash

[baptiste@docker nginx]$ docker run -it python bash
Unable to find image 'python:latest' locally
latest: Pulling from library/python
bc0734b949dc: Pull complete
b5de22c0f5cd: Pull complete
917ee5330e73: Pull complete
b43bd898d5fb: Pull complete
7fad4bffde24: Pull complete
d685eb68699f: Pull complete
107007f161d0: Pull complete
02b85463d724: Pull complete
Digest: sha256:3733015cdd1bd7d9a0b9fe21a925b608de82131aa4f3d397e465a1fcb545d36f
Status: Downloaded newer image for python:latest
root@837c50b52b5a:/#


```

🌞 **Installe des libs Python**


```bash
root@837c50b52b5a:/# pip install aiohttp
Successfully built multidict
Installing collected packages: multidict, idna, frozenlist, attrs, yarl, aiosignal, aiohttp
Successfully installed aiohttp-3.9.1 aiosignal-1.3.1 attrs-23.1.0 frozenlist-1.4.1 idna-3.6 multidict-6.0.4 yarl-1.9.4
WARNING: Running pip as the 'root' user can result in broken permissions and conflicting behaviour with the system package manager. It is recommended to use a virtual environment instead: https://pip.pypa.io/warnings/venv

[notice] A new release of pip is available: 23.2.1 -> 23.3.2
[notice] To update, run: pip install --upgrade pip


root@837c50b52b5a:/# pip install aioconsole
Collecting aioconsole
  Obtaining dependency information for aioconsole from https://files.pythonhosted.org/packages/f7/39/b392dc1a8bb58342deacc1ed2b00edf88fd357e6fdf76cc6c8046825f84f/aioconsole-0.7.0-py3-none-any.whl.metadata
  Downloading aioconsole-0.7.0-py3-none-any.whl.metadata (5.3 kB)
Downloading aioconsole-0.7.0-py3-none-any.whl (30 kB)
Installing collected packages: aioconsole
Successfully installed aioconsole-0.7.0
WARNING: Running pip as the 'root' user can result in broken permissions and conflicting behaviour with the system package manager. It is recommended to use a virtual environment instead: https://pip.pypa.io/warnings/venv

[notice] A new release of pip is available: 23.2.1 -> 23.3.2
[notice] To update, run: pip install --upgrade pip



root@837c50b52b5a:/# python
Python 3.12.1 (main, Dec 19 2023, 20:14:15) [GCC 12.2.0] on linux
Type "help", "copyright", "credits" or "license" for more information.
>>> import aiohttp
>>>

```
