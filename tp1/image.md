# II. Images

## 1. Images publiques

🌞 **Récupérez des images**

```bash
[baptiste@docker ~]$ docker pull python:3.11
3.11: Pulling from library/python
bc0734b949dc: Already exists
b5de22c0f5cd: Already exists
917ee5330e73: Already exists
b43bd898d5fb: Already exists
7fad4bffde24: Already exists
1f68ce6a3e62: Pull complete
e27d998f416b: Pull complete
fefdcd9854bf: Pull complete
Digest: sha256:4e5e9b05dda9cf699084f20bb1d3463234446387fa0f7a45d90689c48e204c83
Status: Downloaded newer image for python:3.11
docker.io/library/python:3.11


[baptiste@docker ~]$ docker pull mysql:5.7
5.7: Pulling from library/mysql
20e4dcae4c69: Pull complete
1c56c3d4ce74: Pull complete
e9f03a1c24ce: Pull complete
68c3898c2015: Pull complete
6b95a940e7b6: Pull complete
...


[baptiste@docker ~]$ docker pull wordpress:latest
latest: Pulling from library/wordpress
Digest: sha256:ffabdfe91eefc08f9675fe0e0073b2ebffa8a62264358820bcf7319b6dc09611
Status: Image is up to date for wordpress:latest
docker.io/library/wordpress:latest

[baptiste@docker ~]$ docker pull linuxserver/wikijs
Using default tag: latest
latest: Pulling from linuxserver/wikijs
8b16ab80b9bd: Pull complete
07a0e16f7be1: Pull complete
145cda5894de: Pull complete
1a16fa4f6192: Pull complete
84d558be1106: Pull complete
4573be43bb06: Pull complete
20b23561c7ea: Pull complete
Digest: sha256:131d247ab257cc3de56232b75917d6f4e24e07c461c9481b0e7072ae8eba3071
Status: Downloaded newer image for linuxserver/wikijs:latest
docker.io/linuxserver/wikijs:latest


[baptiste@docker ~]$ docker image ls
REPOSITORY           TAG       IMAGE ID       CREATED       SIZE
linuxserver/wikijs   latest    869729f6d3c5   6 days ago    441MB
mysql                5.7       5107333e08a8   8 days ago    501MB
python               latest    fc7a60e86bae   13 days ago   1.02GB
wordpress            latest    fd2f5a0c6fba   2 weeks ago   739MB
python               3.11      22140cbb3b0c   2 weeks ago   1.01GB
nginx                latest    d453dd892d93   8 weeks ago   187MB

```

🌞 **Lancez un conteneur à partir de l'image Python**

```bash
[baptiste@docker ~]$ docker run -it --rm python:3.11 python
Python 3.11.7 (main, Dec 19 2023, 20:33:49) [GCC 12.2.0] on linux
Type "help", "copyright", "credits" or "license" for more information.
>>> exit()

```

## 2. Construire une image

🌞 **Ecrire un Dockerfile pour une image qui héberge une application Python**

```
[baptiste@docker python_app_build]$ cat Dockerfile
FROM debian:latest
RUN apt update -y && apt install -y python3 && apt install -y python3-pip && apt install python3-emoji
COPY app.py /app.py
ENTRYPOINT ["python3", "/app.py"]
```


🌞 **Build l'image**
```
[baptiste@docker python_app_build]$ docker build . -t python_app:version_de_ouf
[+] Building 0.5s (8/8) FINISHED
```

🌞 **Lancer l'image**
```
[baptiste@docker python_app_build]$ docker run -t python_app:version_de_ouf
Cet exemple d'application est vraiment naze 👎
```

